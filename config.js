let config = {}

// End video_directory with a /
config.video_directory = ''
// If ffmpeg is in PATH you can just enter 'ffmpeg'
config.ffmpeg_executable = ''
// If ffprobe is in PATH you can just enter 'ffprobe'
config.ffprobe_executable = ''
// Make sure stream URL includes your key
config.stream_url = ''
// Use a secure secret!
config.secret = 'secret'

module.exports = config