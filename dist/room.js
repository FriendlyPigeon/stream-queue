let socket = null
let options = null
let player = null
let subsOn = true

$(function() {
  socket = io()
  socket.emit('join room', window.location.pathname.split("/").pop())
  socket.on('users', (users) => {
    let usersText = ''
    
    users.forEach(user => {
      usersText = usersText + user + '</br>'
    })
    
    let tooltip = document.querySelector('#users-list')
    $(tooltip)
    .attr('data-original-title', usersText)
    
    $('[data-toggle="tooltip"]').tooltip({html: true})
    
    let usersCount = document.querySelector('#users-count')
    usersCount.innerHTML = 'Users: ' + users.length
  })
    
  options = {
    plugins: {
      httpSourceSelector:
      {
        default: 'auto'
      }
    }
  }
  player = videojs('#room-video', options)
  
  socket.on('updated-queue', (queue) => {
    let queuedVideos = document.querySelector('#queued-videos')
    queuedVideos.innerHTML = ""
    queue.forEach(video => {
      let paragraph = document.createElement('p')
      let videoText = document.createTextNode(video.username + ' - ' + video.filename)
      paragraph.appendChild(videoText)
      
      queuedVideos.appendChild(paragraph)
    })
  })
  socket.on('new-video', (video) => {
    let oldTracks = player.remoteTextTracks()
    let i = oldTracks.length
    while(i--) {
      player.removeRemoteTextTrack(oldTracks[i])
    }
    streamQueueFetch()
  })
  socket.on('new-time', time => {
    
    let timer = document.querySelector('#current-time')
    timer.innerHTML = 'Current video time: ' + `${convertToTimeString(time)}`
    
    let slider = document.querySelector('#time')
    slider.value = time
    
    if(window.octopusInstance) {
      let realTime = player.currentTime()
      window.octopusInstance.setCurrentTime(realTime)
    }
  })
  socket.on('new-subs', () => {
    if(window.octopusInstance) {
      window.octopusInstance.dispose()
    }
    
    player.ready(() => {
      window.SubtitlesOctopusOnLoad = function() {
        let options = {
          video: document.getElementById('room-video'),
          subUrl: '/streams/'+window.location.pathname.split('/').pop()+'_subs.ass',
          workerUrl: '/subtitles-octopus-worker.js'
        }
        
        player.ready(() => {
          window.octopusInstance = new SubtitlesOctopus(options)
        })
      }
      if(SubtitlesOctopus) {
        window.SubtitlesOctopusOnLoad()
      }
    })
  })
  socket.on('video-duration', (seconds) => {
    let timeSlider = document.querySelector('#time')
    timeSlider.setAttribute('max', seconds)
    let setTimeSlider = document.querySelector('#set-time-slider')
    if(setTimeSlider) {
      setTimeSlider.setAttribute('max', seconds)
    }
  })
  socket.on('new-message', (messageObj) => {
    let from = messageObj.from
    let message = messageObj.message
    let chatMessages = document.querySelector('#chat-messages')

    let willAutoScroll = true
    let scrollHeight = chatMessages.scrollHeight
    let scrollTop = chatMessages.scrollTop
    let outerHeight = $('#chat-messages').outerHeight()
    if(scrollHeight != scrollTop+Math.round(outerHeight)) {
      willAutoScroll = false
    }

    let newChatMessage = document.createElement('li')
    newChatMessage.appendChild(document.createTextNode(`${from}: ${message}`))
    chatMessages.appendChild(newChatMessage)

    if(willAutoScroll) {
      chatMessages.scrollTo(0, scrollHeight)
    }
  })
})

function convertToTimeString(time) {
  let hours = parseInt(time/3600)
  let minutes = parseInt((time-(hours*3600))/60)
  let seconds = time % 60
  
  if(hours < 10) {
    hours = '0'+hours
  }
  if(minutes < 10) {
    minutes = '0'+minutes
  }
  if(seconds < 10) {
    seconds = '0'+seconds
  }
  return `${hours}:${minutes}:${seconds}`
}

function toggleSubs() {
  if(subsOn) {
    window.octopusInstance.dispose()
    subsOn = false
  } else {
    window.SubtitlesOctopusOnLoad = function() {
      let options = {
        video: document.getElementById('room-video'),
        subUrl: '/streams/'+window.location.pathname.split('/').pop()+'_subs.ass',
        workerUrl: '/subtitles-octopus-worker.js'
      }
      
      player.ready(() => {
        window.octopusInstance = new SubtitlesOctopus(options)
      })
    }
    if(SubtitlesOctopus) {
      window.SubtitlesOctopusOnLoad()
    }

    subsOn = true
  }
}

function streamQueueFetch() {
  let attemptStreamQueueFetch = setInterval(function() {
    fetch(`/streams/${window.location.pathname.split('/').pop()+'.mpd'}`, {
      method: 'GET'
    })
    .then((response) => {
      if(response.status === 200) {
        player.src({
          type: 'application/dash+xml',
          src: '/streams/'+window.location.pathname.split('/').pop()+'.mpd'
        })
        player.httpSourceSelector()
        player.play()
        clearInterval(attemptStreamQueueFetch)
      }
    })
  }, 3000)
}

function enterChatMessage(event) {
  if(event.keyCode == 13 || event.which == 13) {
    let inputText = document.querySelector('#chat-input').value
    socket.emit('send-message', {room: window.location.pathname.split('/').pop(), message: inputText})
    document.querySelector('#chat-input').value = ''
    event.preventDefault()
  }
}

function toggleChat() {
  let videoContainer = document.querySelector('#video-container')
  let chatContainer = document.querySelector('#chat-container')

  if(chatContainer.style.display === 'flex') {
      chatContainer.style.display = 'none'
      videoContainer.style.width = '100%'
      return
  }

  chatContainer.style.display = 'flex'
  videoContainer.style.width = '75%'
}