
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        // Password is password1 with secret as "secret"
        // CHANGE PASSWORD HASH TO MATCH YOUR SECRET TO LOGIN!
        { username: 'pigeon', password_hash: '$2b$10$IY6kz4LAn0MLRb1sJHDAzui8T.VEKVBpIPXDU1We7LDEuWqgigQsi', is_admin: 1, is_uploader: 1, is_queuer: 1 },
        // Password is password2 with secret as "secret"
        // CHANGE PASSWORD HASH TO MATCH YOUR SECRET TO LOGIN!
        { username: 'bob', password_hash: '$2b$10$KnlOLQ04YjJRCIEf9HPcBuXzSCvmabBzq2vIKyL/cj/4soSLQjLz2', is_admin: 0, is_uploader: 1, is_queuer: 1 },
        // Password is password3 with secret as "secret"
        // CHANGE PASSWORD HASH TO MATCH YOUR SECRET TO LOGIN!
        { username: 'tom', password_hash: '$2b$10$AmwYlU9vFrMix0gUJCjsZu7r96IB17NX0fDcWS//MJEBnzOquv8aC', is_admin: 0, is_uploader: 0, is_queuer: 1 },
      ]);
    });
};
