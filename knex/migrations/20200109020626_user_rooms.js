
exports.up = function(knex) {
  return knex.schema
  .table('users', function(table) {
    table.text('stream_url')
  })
};

exports.down = function(knex) {
  return knex.schema
  .table('users', function(table) {
    table.dropColumn('stream_url')
  })
};
