
exports.up = function(knex) {
  return knex.schema
  .createTable('users', function(table) {
      table.increments()
      table.text('username').unique().notNullable()
      table.text('password_hash').notNullable()
      table.integer('is_admin').notNullable().defaultTo(0)
      table.integer('is_uploader').notNullable().defaultTo(0)
      table.integer('is_queuer').notNullable().defaultTo(0)
  })
};

exports.down = function(knex) {
  return knex.schema
  .dropTable('users')
};
