
exports.up = function(knex) {
    return knex.schema
    .table('users', function(table) {
      table.text('stream_link')
    })
  };
  
  exports.down = function(knex) {
    return knex.schema
    .table('users', function(table) {
      table.dropColumn('stream_link')
    })
  };
  