const path = require('path')
const fs = require('fs')
const treeKill = require('tree-kill')

const express = require('express')
const cors = require('cors')
const app = express()
const http = require('http').createServer(app)
global.io = require('socket.io')(http)
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const knex = require('./knex/knex.js')

const session = require('express-session')
const KnexSessionStore = require('connect-session-knex')(session)
const store = new KnexSessionStore()

const bcrypt = require('bcrypt')
const saltRounds = 10

const { exec, spawn, execSync } = require('child_process')

const config = require('./config')

app.use(cors())
let staticOptions = {
    setHeaders: function(res, path, stat) {
        if(path.split('.').pop() === 'm4s') {
            res.setHeader('Cache-Control', 'public, max-age=60')
        }
    }
}
app.use(express.static(__dirname + '/dist', staticOptions))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(fileUpload())
app.set('view engine', 'ejs')

let sessionMiddleware = session({
    store: store,
    resave: false,
    saveUninitialized: false,
    secret: config.secret,
    cookie: { maxAge: 7*24*60*60*1000 } // 1 week
})
io.use(function(socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next)
})
app.use(sessionMiddleware)

app.locals.isLoggedIn = 0
app.locals.username = null
app.locals.isAdmin = 0
app.locals.isUploader = 0
app.locals.isQueuer = 0

const port = (config.port || 3000)

let videoQueues = []

let runningVideoProcs = []
let runningVideoTimes = []
let runningVideoOffsets = []
let runningVideoDurations = []

let usersInRooms = []

let wasSetToTime = false

io.on('connection', (socket) => {
    socket.on('join room', (room) => {
        socket.join('room:'+room)

        let roomQueue = videoQueues.find(obj => {
            return obj.username === room
        })

        let usersInRoom = usersInRooms.find(obj => {
            return obj.room === room
        })

        let runningVideoDuration = runningVideoDurations.find(obj => {
            return obj.room === room
        })

        if(runningVideoDuration) {
            socket.emit('video-duration', runningVideoDuration.duration)
        }

        if(usersInRoom === undefined) {
            usersInRooms.push({
                room: room,
                users: socket.request.session.username ? [socket.request.session.username] : ['guest']
            })

            usersInRoom = usersInRooms.find(obj => {
                return obj.room === room
            })
        } else {
            usersInRoom.users.push(socket.request.session.username ? socket.request.session.username : 'guest')
        }

        if(roomQueue !== undefined && roomQueue.roomQueue[0] !== undefined) {
            roomQueue = roomQueue.roomQueue
            socket.emit('updated-queue', roomQueue)
            socket.emit('new-video', roomQueue[0])
            if(roomQueue[0].hasSubs) {
                socket.emit('new-subs')
            }
        }

        io.in('room:'+room).emit('users', usersInRoom.users)
    })

    socket.on('disconnecting', () => {
        if(Object.keys(socket.rooms)[1]) {
            let room = usersInRooms.find(obj => {
                return obj.room === Object.keys(socket.rooms)[1].substr(5)
            })
            
            if(room) {
                let roomUsers = room.users
    
                if(socket.request.session.username) {
                    let index = roomUsers.indexOf(socket.request.session.username)
                    roomUsers.splice(index, 1)
                } else {
                    let index = roomUsers.indexOf('guest')
                    roomUsers.splice(index, 1)
                }
    
                room.users = roomUsers

                io.in(Object.keys(socket.rooms)[1]).emit('users', room.users)
            }
        }
    })

    socket.on('send-message', (messageObj) => {
        io.in('room:'+messageObj.room).emit('new-message', {
            from: socket.request.session.username ? socket.request.session.username : 'guest',
            message: messageObj.message
        })
    })
})

function parseSingleQuote(path) {
    if(path.indexOf("\'") === -1) {
        return path
    } else {
        let regex = /'/gi
        return path.replace(regex, "'\\\\\\\''")
    }
}

function convertToWindowsPathFfmpeg(path) {
    return path.slice(0, 1) + '\\' + path.slice(1)
}

function buildFfmpegSpawnArgArray(username, filename) {
    let mapCounter = 0
    let argsArray = [
        '-hide_banner',
        '-re',
        '-i',
        filename
    ]

    if(config.transcodeLQ) {
        argsArray.push(
            '-map', '0:v:0'
        )
    }
    if(config.transcodeHQ) {
        argsArray.push(
            '-map', '0:v:0'
        )
    }
    if(config.transcodeNative) {
        argsArray.push(
            '-map', '0:v:0'
        )
    }

    if(config.transcodeLQ) {
        argsArray.push(
            '-b:v:'+mapCounter,
            config.transcodeLQBitrate,
            '-c:v:'+mapCounter,
            'libx264',
            '-filter:v:'+mapCounter,
            'format=yuv420p,scale=-2:480'
        )
        mapCounter++
    }
    if(config.transcodeHQ) {
        argsArray.push(
            '-b:v:'+mapCounter,
            config.transcodeHQBitrate,
            '-c:v:'+mapCounter,
            'libx264',
            '-filter:v:'+mapCounter,
            'format=yuv420p,scale=-2:720'
        )
        mapCounter++
    }
    if(config.transcodeNative) {
        argsArray.push(
            '-c:v:'+mapCounter,
            'copy'
        )
    }

    argsArray.push(
        `-map`, `0:a`,
        `-c:a`, `aac`,
        `-b:a`, `192k`,
        `-ac`, `2`,
        `-keyint_min`, `24`,
        `-g`, `48`,
        `-init_seg_name`, `${username}_init-stream$RepresentationID$.$ext$`,
        `-media_seg_name`, `${username}_chunk-stream$RepresentationID$-$Number%05d$.$ext$`,
        `-f`, `dash`,
        `dist/streams/${username}.mpd`
    )

    return argsArray
}

function playRoomVideo(username) {
    let room = videoQueues.find(obj => {
        return obj.username === username
    })
    
    let roomQueue = room.roomQueue
    let runningYoutubeDlProcLocal = null
    let runningVideoProcLocal = null
    let extractSubtitlesProc = null
    let getVideoTimeProc = null
    
    if(roomQueue[0].filename.substr(0, 8) === 'https://' || roomQueue[0].filename.substr(0, 7) === 'http://') {
        runningYoutubeDlProcLocal = spawn(`${config.youtubeDl_executable}`, [`-f`, `best`, `${roomQueue[0].filename}`, `--no-continue`, `-o`, `dist/streams/${username}.mp4`])

        runningYoutubeDlProcLocal.on('exit', () => {
            getVideoTimeProc = spawn(`${config.ffprobe_executable}`, [`-v`, `error`, `-show_entries`, `format=duration`, `-of`, `default=noprint_wrappers=1:nokey=1`, `dist/streams/${username}.mp4`])

            getVideoTimeProc.stdout.on('data', (data) => {
                let duration = parseInt(data.toString())

                let runningVideoDuration = runningVideoDurations.find(obj => {
                    return obj.room === username
                })

                if(runningVideoDuration) {
                    runningVideoDuration.duration = duration
                } else {
                    runningVideoDurations.push({
                        room: username,
                        duration: duration
                    })
                }
                io.in('room:'+username).emit('video-duration', parseInt(data.toString()))
            })

            runningVideoProcLocal = spawn(`${config.ffmpeg_executable}`, [
                `-hide_banner`,
                `-re`,
                `-i`, `dist/streams/${username}.mp4`,
                `-ac`, `2`,
                `-c:v`, `copy`,
                `-c:a`, `aac`,
                `-b:a`, `192k`,
                `-init_seg_name`, `${username}_init-stream$RepresentationID$.$ext$`,
                `-media_seg_name`, `${username}_chunk-stream$RepresentationID$-$Number%05d$.$ext$`,
                `-f`, `dash`,
                `dist/streams/${username}.mpd`
            ])

            let roomRunningVideoProc = runningVideoProcs.find(obj => {
                return obj.username === username
            })
            roomRunningVideoProc.roomVideoProc = runningVideoProcLocal

            extractSubtitlesProc = spawn(`${config.youtubeDl_executable}`, [`--write-sub`, `--sub-format`, `vtt`, `--sub-lang`, `en`, `--skip-download`, `${roomQueue[0].filename}`, `-o`, `dist/streams/youtube-${username}_subs.%(ext)s`])

            extractSubtitlesProc.on('exit', () => {
                if(roomQueue[0] !== undefined) {
                    if(fs.existsSync(`dist/streams/youtube-${username}_subs.en.vtt`)) {
                        roomQueue[0].hasSubs = true
        
                        let convertSubtitlesProc = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-i`, `dist/streams/youtube-${username}_subs.en.vtt`, `-map`, `0:s:0`, `dist/streams/${username}_subs.ass`])
        
                        convertSubtitlesProc.on('exit', () => {
                            io.in('room:'+username).emit('new-subs')
                        })
                    } else {
                        roomQueue[0].hasSubs = false
                    }
                }
            })

            io.in('room:'+username).emit('new-video', roomQueue[0])
            io.in('room:'+username).emit('updated-queue', roomQueue)

            let roomRunningVideoTime = runningVideoTimes.find(obj => {
                return obj.username === username
            })

            if(roomRunningVideoTime) {
                roomRunningVideoTime.roomVideoTime = 0
            } else {
                runningVideoTimes.push({
                    username: username,
                    roomVideoTime: 0
                })
                
                roomRunningVideoTime = runningVideoTimes.find(obj => {
                    return obj.username === username
                })
            }

            extractSubtitlesProc.on('exit', () => {
                io.in('room:'+username).emit('new-subs')
            })

            runningVideoProcLocal.stderr.on('data', function(data) {
                if(data.toString().match(/time=..:..:..\.../g)) {
                    let timeString = data.toString().match(/time=..:..:..\.../g)[0].replace('time=', '')
                    let timeInSeconds = (parseInt(timeString.substr(0,2))*3600)+(parseInt(timeString.substr(3,2))*60)+(parseInt(timeString.substr(6,2)))
                    roomRunningVideoTime.roomVideoTime = timeInSeconds
                    io.in('room:'+username).emit('new-time', timeInSeconds)
                }
            })

            runningVideoProcLocal.on('exit', () => {
                let regex = new RegExp(`^${username}.*$`)
                fs.readdirSync('./dist/streams/')
                .filter(f => regex.test(f))
                .map(f => {
                    try {
                        if(f !== `${username}.mp4`) {
                            fs.unlinkSync('./dist/streams/'+f)
                        }
                    } catch {
                        console.error('Failed to remove file.')
                    }
                })

                roomQueue.splice(0, 1)
                io.in('room:'+username).emit('updated-queue', roomQueue)
                if(roomQueue.length !== 0 && !wasSetToTime) {
                    let roomVideoProc = runningVideoProcs.find(obj => {
                        return obj.username === username
                    })

                    roomVideoProc.roomVideoProc = playRoomVideo(username)
                } else {
                    wasSetToTime = false
                }
                return
            })

            return
        })

        return runningYoutubeDlProcLocal
    } else {
        const ffprobe_subtitles = execSync(`${config.ffprobe_executable} -loglevel error -select_streams s -show_entries stream=index:stream_tags=language -of csv=p=0 "${config.video_directory}${roomQueue[0].filename}"`).toString()
        if(ffprobe_subtitles === "") {
            roomQueue[0].hasSubs = false
        } else {
            roomQueue[0].hasSubs = true
            extractSubtitlesProc = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-i`, `${config.video_directory}${roomQueue[0].filename}`, `-map`, `0:s:0`, `dist/streams/${username}_subs.ass`])

            extractSubtitlesProc.on('exit', () => {
                io.in('room:'+username).emit('new-subs')
            })
        }
        
        getVideoTimeProc = spawn(`${config.ffprobe_executable}`, [`-v`, `error`, `-show_entries`, `format=duration`, `-of`, `default=noprint_wrappers=1:nokey=1`, `${config.video_directory}${roomQueue[0].filename}`])

        getVideoTimeProc.stdout.on('data', (data) => {
            let duration = parseInt(data.toString())

            let runningVideoDuration = runningVideoDurations.find(obj => {
                return obj.room === username
            })

            if(runningVideoDuration) {
                runningVideoDuration.duration = duration
            } else {
                runningVideoDurations.push({
                    room: username,
                    duration: duration
                })
            }
            io.in('room:'+username).emit('video-duration', parseInt(data.toString()))
        })

        //const ffprobe_streams = execSync(`${config.ffprobe_executable} "${config.video_directory}${roomQueue[0].filename}" -show_entries stream=index:stream_tags=languages -select_streams a -of compact=p=0:nk=1`)
        runningVideoProcLocal = spawn(`${config.ffmpeg_executable}`, buildFfmpegSpawnArgArray(username, `${config.video_directory}${roomQueue[0].filename}`))
        io.in('room:'+username).emit('new-video', roomQueue[0])
        io.in('room:'+username).emit('updated-queue', roomQueue)

        let roomRunningVideoTime = runningVideoTimes.find(obj => {
            return obj.username === username
        })

        if(roomRunningVideoTime) {
            roomRunningVideoTime.roomVideoTime = 0
        } else {
            runningVideoTimes.push({
                username: username,
                roomVideoTime: 0
            })
            
            roomRunningVideoTime = runningVideoTimes.find(obj => {
                return obj.username === username
            })
        }

        runningVideoProcLocal.stderr.on('data', function(data) {
            console.log(data.toString())
            if(data.toString().match(/time=..:..:..\.../g)) {
                let timeString = data.toString().match(/time=..:..:..\.../g)[0].replace('time=', '')
                let timeInSeconds = (parseInt(timeString.substr(0,2))*3600)+(parseInt(timeString.substr(3,2))*60)+(parseInt(timeString.substr(6,2)))
                roomRunningVideoTime.roomVideoTime = timeInSeconds
                io.in('room:'+username).emit('new-time', timeInSeconds)
            }
        })

        runningVideoProcLocal.on('exit', () => {
            let regex = new RegExp(`${username}.*$`)
            fs.readdirSync('./dist/streams/')
            .filter(f => regex.test(f))
            .map(f => {
                try {
                    fs.unlinkSync('./dist/streams/'+f)
                } catch {
                    console.error('Failed to remove file.')
                }
            })

            roomQueue.splice(0, 1)
            io.in('room:'+username).emit('updated-queue', roomQueue)
            if(roomQueue.length !== 0 && !wasSetToTime) {
                let roomVideoProc = runningVideoProcs.find(obj => {
                    return obj.username === username
                })

                roomVideoProc.roomVideoProc = playRoomVideo(username)
            } else {
                wasSetToTime = false
            }
            return
        })

        return runningVideoProcLocal
    }
}

function skipVideo(room) {
    let roomRunningVideoProc = runningVideoProcs.find(obj => {
        return obj.username === room
    })

    if(roomRunningVideoProc) {
        roomRunningVideoProc.roomVideoProc.kill('SIGKILL')
    }
}

function resetVideo(room) {
    wasSetToTime = true

    let roomRunningVideoProc = runningVideoProcs.find(obj => {
        return obj.username === room
    })

    if(roomRunningVideoProc) {
        let roomQueue = videoQueues.find(obj => {
            return obj.username === room
        })

        // Add copy of video to beginning of queue so when the process
        // is killed, the same video is still in the queue
        if(roomQueue) {
            roomQueue.roomQueue.splice(0, 0, roomQueue.roomQueue[0])
        }

        roomRunningVideoProc.roomVideoProc.kill('SIGKILL')
    }
}

function playRoomVideoAtTime(username, seconds) {
    let room = videoQueues.find(obj => {
        return obj.username === username
    })
    
    let roomQueue = room.roomQueue
    let runningVideoProcLocal = null
    let extractSubtitlesProc = null
    let getVideoTimeProc = null
    
    if(roomQueue[0].filename.substr(0, 8) === 'https://' || roomQueue[0].filename.substr(0, 7) === 'http://') {
        runningVideoProcLocal = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-ss`, `${seconds}`, `-re`, `-i`, `dist/streams/${username}.mp4`, `-ac`, `2`, `-c:v`, `copy`, `-c:a`, `aac`, `-b:a`, `192k`, `-init_seg_name`, `${username}_init-stream$RepresentationID$.$ext$`, `-media_seg_name`, `${username}_chunk-stream$RepresentationID$-$Number%05d$.$ext$`, `-f`, `dash`, `dist/streams/${username}.mpd`])
        extractSubtitlesProc = spawn(`${config.youtubeDl_executable}`, [`--write-sub`, `--sub-format`, `vtt`, `--sub-lang`, `en`, `--skip-download`, `${roomQueue[0].filename}`, `-o`, `dist/streams/youtube-${username}_subs.%(ext)s`])

        extractSubtitlesProc.on('exit', () => {
            if(roomQueue[0] !== undefined) {
                if(fs.existsSync(`dist/streams/youtube-${username}_subs.en.vtt`)) {
                    roomQueue[0].hasSubs = true
    
                    let convertSubtitlesProc = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-ss`, `${seconds}`, `-i`, `dist/streams/youtube-${username}_subs.en.vtt`, `-map`, `0:s:0`, `dist/streams/${username}_subs.ass`])
    
                    convertSubtitlesProc.on('exit', () => {
                        io.in('room:'+username).emit('new-subs')
                    })
                } else {
                    roomQueue[0].hasSubs = false
                }
            }
        })

        io.in('room:'+username).emit('new-video', roomQueue[0])
        io.in('room:'+username).emit('updated-queue', roomQueue)

        let roomRunningVideoTime = runningVideoTimes.find(obj => {
            return obj.username === username
        })

        if(roomRunningVideoTime) {
            roomRunningVideoTime.roomVideoTime = 0
        } else {
            runningVideoTimes.push({
                username: username,
                roomVideoTime: 0
            })
            
            roomRunningVideoTime = runningVideoTimes.find(obj => {
                return obj.username === username
            })
        }

        runningVideoProcLocal.stderr.on('data', function(data) {
            if(data.toString().match(/time=..:..:..\.../g)) {
                let timeString = data.toString().match(/time=..:..:..\.../g)[0].replace('time=', '')
                let timeInSeconds = (parseInt(seconds))+(parseInt(timeString.substr(0,2))*3600)+(parseInt(timeString.substr(3,2))*60)+(parseInt(timeString.substr(6,2)))
                roomRunningVideoTime.roomVideoTime = timeInSeconds
                io.in('room:'+username).emit('new-time', timeInSeconds)
            }
        })

        runningVideoProcLocal.on('exit', () => {
            let regex = new RegExp(`^${username}.*$`)
            fs.readdirSync('./dist/streams/')
            .filter(f => regex.test(f))
            .map(f => {
                try {
                    if(f !== `${username}.mp4`) {
                        fs.unlinkSync('./dist/streams/'+f)
                    }
                } catch {
                    console.error('Failed to remove file.')
                }
            })

            roomQueue.splice(0, 1)
            io.in('room:'+username).emit('updated-queue', roomQueue)
            if(roomQueue.length !== 0 && !wasSetToTime) {
                let roomVideoProc = runningVideoProcs.find(obj => {
                    return obj.username === username
                })

                roomVideoProc.roomVideoProc = playRoomVideo(username)
            } else {
                wasSetToTime = false
            }
            return
        })

        return runningVideoProcLocal
    } else {
        const ffprobe_stdout = execSync(`${config.ffprobe_executable} -loglevel error -select_streams s -show_entries stream=index:stream_tags=language -of csv=p=0 "${config.video_directory}${roomQueue[0].filename}"`).toString()
        if(ffprobe_stdout === "") {
            roomQueue[0].hasSubs = false
        } else {
            roomQueue[0].hasSubs = true
            extractSubtitlesProc = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-ss`, `${seconds}`, `-i`, `${config.video_directory}${roomQueue[0].filename}`, `-map`, `0:s:0`, `dist/streams/${username}_subs.ass`])

            extractSubtitlesProc.on('exit', () => {
                io.in('room:'+username).emit('new-subs')
            })
        }

        getVideoTimeProc = spawn(`${config.ffprobe_executable}`, [`-v`, `error`, `-show_entries`, `format=duration`, `-of`, `default=noprint_wrappers=1:nokey=1`, `${config.video_directory}${roomQueue[0].filename}`])

        getVideoTimeProc.stdout.on('data', (data) => {
            let duration = parseInt(data.toString())

            let runningVideoDuration = runningVideoDurations.find(obj => {
                return obj.room === username
            })

            if(runningVideoDuration) {
                runningVideoDuration.duration = duration
            } else {
                runningVideoDurations.push({
                    room: username,
                    duration: duration
                })
            }
            io.in('room:'+username).emit('video-duration', parseInt(data.toString()))
        })

        runningVideoProcLocal = spawn(`${config.ffmpeg_executable}`, [`-hide_banner`, `-ss`, `${seconds}`, `-re`, `-i`, `${config.video_directory}${roomQueue[0].filename}`, `-c:v`, `copy`, `-map`, `0:v`, `-b:v`, `1000k`, `-c:a`, `aac`, `-map`, `0:a`, `-b:a`, `192k`, `-ac`, `2`, `-keyint_min`, `24`, `-g`, `48`, `-init_seg_name`, `${username}_init-stream$RepresentationID$.$ext$`, `-media_seg_name`, `${username}_chunk-stream$RepresentationID$-$Number%05d$.$ext$`, `-f`, `dash`, `dist/streams/${username}.mpd`])
        io.in('room:'+username).emit('new-video', roomQueue[0])
        io.in('room:'+username).emit('updated-queue', roomQueue)

        let roomRunningVideoTime = runningVideoTimes.find(obj => {
            return obj.username === username
        })

        if(roomRunningVideoTime) {
            roomRunningVideoTime.roomVideoTime = 0
        } else {
            runningVideoTimes.push({
                username: username,
                roomVideoTime: 0
            })
            
            roomRunningVideoTime = runningVideoTimes.find(obj => {
                return obj.username === username
            })
        }

        runningVideoProcLocal.stderr.on('data', function(data) {
            if(data.toString().match(/time=..:..:..\.../g)) {
                let timeString = data.toString().match(/time=..:..:..\.../g)[0].replace('time=', '')
                let timeInSeconds = (parseInt(seconds))+(parseInt(timeString.substr(0,2))*3600)+(parseInt(timeString.substr(3,2))*60)+(parseInt(timeString.substr(6,2)))
                roomRunningVideoTime.roomVideoTime = timeInSeconds
                io.in('room:'+username).emit('new-time', timeInSeconds)
            }
        })

        runningVideoProcLocal.on('exit', () => {
            let regex = new RegExp(`${username}.*$`)
            fs.readdirSync('./dist/streams/')
            .filter(f => regex.test(f))
            .map(f => {
                try {
                    fs.unlinkSync('./dist/streams/'+f)
                } catch {
                    console.error('Failed to remove file.')
                }
            })

            roomQueue.splice(0, 1)
            io.in('room:'+username).emit('updated-queue', roomQueue)
            if(roomQueue.length !== 0 && !wasSetToTime) {
                let roomVideoProc = runningVideoProcs.find(obj => {
                    return obj.username === username
                })

                roomVideoProc.roomVideoProc = playRoomVideo(username)
            } else {
                wasSetToTime = false
            }
            return
        })

        return runningVideoProcLocal
    }
}

const isUploader = () => {
    return(req, res, next) => {
        if(req.session.is_uploader === 1 || req.session.is_admin === 1) {
            next()
        } else {
            res.render('pages/noauth', { unauthorizedAction: 'upload a file.' })
        }
    }
}

const isQueuer = () => {
    return(req, res, next) => {
        if(req.session.is_queuer === 1 || req.session.is_admin === 1) {
            next()
        } else {
            res.render('pages/noauth', { unauthorizedAction: 'view the queue.' })
        }
    }
}

const isQueuerOrUploader = () => {
    return(req, res, next) => {
        if(req.session.is_queuer === 1 || req.session.is_uploader || req.session.is_admin === 1) {
            next()
        } else {
            res.render('pages/noauth', { unauthorizedAction: 'view the queue.' })
        }
    }
}

const isAdmin = () => {
    return(req, res, next) => {
        if(req.session.is_admin === 1) {
            next()
        } else {
            res.render('pages/noauth', { unauthorizedAction: 'view the admin panel.' })
        }
    }
}

app.get('*', (req, res, next) => {
    app.locals.isLoggedIn = req.session.username ? 1 : 0
    app.locals.username = req.session.username || null
    app.locals.isAdmin = req.session.is_admin || 0
    app.locals.isUploader = req.session.is_uploader || 0
    app.locals.isQueuer = req.session.is_queuer || 0
    
    next()
})

app.get('/', (req, res) => {
    if(videoQueues.length !== 0) {
        let rooms = videoQueues.filter(room => {
            if(room.roomQueue.length !== 0) {
                return true
            } else {
                return false
            }
        }).map(room => {
            return room.username
        })
        
        if(rooms.length === 0) {
            res.render('pages/index', { rooms: [] })
        } else {
            knex.select('users.username', 'users.stream_link')
            .from('users')
            .whereIn('users.username', rooms)
            .then((users) => {
                rooms = []
                
                users.forEach(user => {
                    rooms.push({
                        username: user.username,
                        streamLink: user.stream_link
                    })
                })
                
                res.render('pages/index', { rooms: rooms })
            })
        }
    } else {
        let rooms = []
        
        res.render('pages/index', { rooms: rooms })
    }
})

app.get('/room/:username', (req, res) => {
    knex.select('users.username')
    .from('users')
    .where('users.username', req.params.username)
    .first()
    .then(function(user) {
        if(user === undefined) {
            res.render('pages/room', { videoLength: 0, runningVideoTime: 0, videoQueue: [], userExists: false })
        } else {
            let roomQueue = videoQueues.find(obj => {
                return obj.username === req.params.username
            })
            
            if(roomQueue) {
                roomQueue = roomQueue.roomQueue
            } else {
                roomQueue = []
            }
            
            let roomVideoTime = runningVideoTimes.find(obj => {
                return obj.username === req.params.username
            })
            
            if(roomVideoTime) {
                roomVideoTime = roomVideoTime.roomVideoTime
            } else {
                runningVideoTimes.push({
                    username: req.params.username,
                    roomVideoTime: 0
                })
                
                roomVideoTime = runningVideoTimes.find(obj => {
                    return obj.username === req.params.username
                }).roomVideoTime
            }
            
            res.render('pages/room', { videoLength: 0, runningVideoTime: roomVideoTime, videoQueue: roomQueue, userExists: true })
        }
    })
})

app.get('/admin', isAdmin(), (req, res) => {
    knex.select('users.id', 'users.username', 'users.is_admin', 'users.is_uploader', 'users.is_queuer')
    .from('users')
    .then(function(users) {
        res.render('pages/admin', { users: users })
    })
})

app.post('/admin/roles/:username', isAdmin(), (req, res) => {
    let is_admin = req.body.is_admin === 'on' ? true : false
    let is_uploader = req.body.is_uploader === 'on' ? true : false
    let is_queuer = req.body.is_queuer === 'on' ? true : false
    
    knex.update({
        is_admin: is_admin,
        is_uploader: is_uploader,
        is_queuer: is_queuer,
    })
    .into('users')
    .where('users.username', req.params.username)
    .then(() => {
        res.redirect('/admin')
    })
})

app.get('/settings', (req, res) => {
    knex.select('users.stream_url', 'users.stream_link')
    .from('users')
    .where('users.username', req.session.username)
    .first()
    .then((user) => {
        res.render('pages/settings', { streamUrl: user.stream_url, streamLink: user.stream_link })
    })
})

app.post('/settings', (req, res) => {
    if(req.session.username && req.body.streamUrl && req.body.streamLink) {
        knex.update({
            stream_url: req.body.streamUrl,
            stream_link: req.body.streamLink
        })
        .into('users')
        .where('users.username', req.session.username)
        .then(() => {
            res.redirect('/')
        })
    } else {
        res.redirect('/')
    }
})

app.get('/login', (req, res) => {
    if(req.session.username) {
        res.redirect('/')
    } else {
        res.render('pages/login', { error: null })
    }
})

app.post('/login', (req, res) => {
    knex.select('users.username', 'users.password_hash', 'users.is_admin', 'users.is_uploader', 'users.is_queuer')
    .from('users')
    .where('users.username', req.body.username)
    .first()
    .then(function(user) {
        if(user === undefined) {
            res.render('pages/login', { error: 'User does not exist or password is invalid' })
        } else {
            bcrypt.compare(req.body.password, user.password_hash, function(err, success) {
                if(success) {
                    req.session.username = user.username
                    req.session.is_admin = user.is_admin
                    req.session.is_uploader = user.is_uploader
                    req.session.is_queuer = user.is_queuer
                    
                    app.locals.isLoggedIn = true
                    app.locals.username = user.username
                    app.locals.isAdmin = user.is_admin
                    app.locals.isUploader = user.is_uploader
                    app.locals.isQueuer = user.is_queuer
                    
                    res.redirect('/')
                } else {
                    res.render('pages/login', { error: 'User does not exist or password is invalid' })
                }
            })
        }
    })
})

app.get('/register', (req, res) => {
    if(req.session.username) {
        res.redirect('/')
    }
    res.render('pages/register', { invalidUsername: false, mismatchedPasswords: false })
})

app.post('/register', (req, res) => {
    knex('users')
    .count('* as count')
    .first()
    .then(function(total) {
        if(total.count === 0 && req.body.password === req.body.confirmPassword) {
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    knex('users')
                    .insert({
                        username: req.body.username,
                        password_hash: hash,
                        is_admin: 1,
                        is_uploader: 1,
                        is_queuer: 1
                    })
                    .then(() => {
                        res.redirect('/login')
                    })
                })
            })
        }
        knex.select('users.username')
        .from('users')
        .where('users.username', req.body.username)
        .first()
        .then(function(user) {
            if(user && req.body.password !== req.body.confirmPassword) {
                res.render('pages/register', { invalidUsername: true, mismatchedPasswords: true })
            } else if(user) {
                res.render('pages/register', { invalidUsername: true, mismatchedPasswords: false})
            } else if(req.body.password !== req.body.confirmPassword) {
                res.render('pages/register', { invalidUsername: false, mismatchedPasswords: true})
            } else if(req.body.password.length < 8) {
                res.render('pages/register', { invalidUsername: false, mismatchedPasswords: true})
            } else {
                bcrypt.genSalt(saltRounds, function(err, salt) {
                    bcrypt.hash(req.body.password, salt, function(err, hash) {
                        knex('users')
                        .insert({
                            username: req.body.username,
                            password_hash: hash,
                            is_admin: 0,
                            is_uploader: 0,
                            is_queuer: 0
                        })
                        .then(() => {
                            res.redirect('/login')
                        })
                    })
                })
            }
        })
    })
})

app.get('/logout', (req, res) => {
    delete req.session.username
    delete req.session.is_admin
    delete req.session.is_uploader
    delete req.session.is_queuer
    
    app.locals.isLoggedIn = 0
    app.locals.username = null
    app.locals.isAdmin = 0
    app.locals.isUploader = 0
    app.locals.isQueuer = 0
    res.redirect('/')
})

app.get('/files', isQueuerOrUploader(), (req, res) => {
    knex.select('users.username')
    .from('users')
    .whereNot('users.is_queuer', null)
    .then(function(users) {
        let directoryList = []
        let fileList = []
        const rootDirectory = `${config.video_directory}`
        let filePath = req.query.filepath
        const normalizedPath = path.normalize(filePath ? filePath : '').replace(/^(\.\.(\/|\\|$))+/, '')
        const resolvedPath = path.join(rootDirectory, normalizedPath)
        
        fs.readdir(resolvedPath, {withFileTypes: true}, function(err, items) {
            if(err) {
                console.error(err)
                return
            }
            for(i = 0; i < items.length; i++) {
                if(items[i].isDirectory()) {
                    directoryList.push(items[i].name+'/')
                }
                else if(items[i].isFile()) {
                    fileList.push(items[i].name)
                }
            }
            res.render('pages/files', { fileList: fileList, directoryList: directoryList, rooms: users })
        })
    })
})

app.post('/queue', isQueuer(), (req, res) => {
    if(req.body.filename && req.body.room) {
        let filename = decodeURIComponent(req.body.filename)
        let room = req.body.room
        
        let roomQueue = videoQueues.find(obj => {
            return obj.username === room
        })
        
        knex.select('users.stream_url')
        .from('users')
        .where('users.username', room)
        .first()
        .then((user) => {
            let streamUrl = user.stream_url
            
            if(roomQueue) {
                roomQueue.roomQueue.push({
                    username: req.session.username,
                    filename: filename
                })
            } else {
                videoQueues.push({
                    username: room,
                    streamUrl: streamUrl,
                    roomQueue: [{
                        username: req.session.username,
                        filename: filename
                    }]
                })
                
                roomQueue = videoQueues.find(obj => {
                    return obj.username === room
                })
            }

            io.in('room:'+room).emit('updated-queue', roomQueue.roomQueue)
            
            if(roomQueue.roomQueue.length === 1) {
                let roomRunningVideoProc = runningVideoProcs.find(obj => {
                    return obj.username === room
                })
                
                if(roomRunningVideoProc) {
                    roomRunningVideoProc.roomVideoProc = playRoomVideo(room)
                } else {
                    runningVideoProcs.push({
                        username: room,
                        roomVideoProc: playRoomVideo(room)
                    })
                }
            }

            res.json('Successfully added to queue!')
        })
    }
})

app.get('/queue/:room/time', (req, res) => {
    let roomTime = runningVideoTimes.find(obj => {
        return obj.username === req.params.room
    })
    
    if(roomTime) {
        res.json({
            time: roomTime.roomVideoTime
        })
    } else {
        res.json({
            time: 0
        })
    }
})

app.post('/queue/:room/settime', isQueuer(), (req, res) => {
    let room = req.params.room
    let seconds = req.body.seconds
    resetVideo(room)
    if(req.body.seconds) {
        let roomRunningVideoProc = runningVideoProcs.find(obj => {
            return obj.username === room
        })
        
        if(roomRunningVideoProc) {
            roomRunningVideoProc.roomVideoProc = playRoomVideoAtTime(room, seconds)
            console.log('settime', roomRunningVideoProc)
        } else {
            runningVideoProcs.push({
                username: room,
                roomVideoProc: playRoomVideoAtTime(room, seconds)
            })
            console.log('settime', roomRunningVideoProc)
        }
        res.json(`Successfully reset video at ${req.body.seconds} seconds!`)
    } else {
        res.json('Please send a JSON POST request with "seconds" field in body')
    }
})

app.post('/queue/skip', isQueuer(), (req, res) => {
    skipVideo(req.body.room)
    res.json('Successfully skipped video!')
})

app.post('/upload', isUploader(), (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    } else if([
        'video/mpeg',
        'video/ogg',
        'video/webm',
        'video/mp4',
        'video/x-matroska'
    ].includes(req.files.videoFile.mimetype) === false)
    {
        return res.status(400).send({ error: 'Uploaded file must be of type mpeg, ogg, webm, mp4, or mkv' })
    }
    
    let videoFile = req.files.videoFile
    
    videoFile.mv(`${config.video_directory}/uploads/${Date.now().toString()}-${videoFile.name}`, function(err) {
        if(err) {
            return res.status(500).send(err)
        }
        res.redirect('back')
    })
})

http.listen(port, () => console.log(`Example app listening on port ${port}!`))